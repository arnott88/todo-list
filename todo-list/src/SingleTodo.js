import React, { useEffect, useState } from 'react'

const SingleTodo = (props) => {
const { index, singleItem, todoList, setTodo } = props
const [display, setDisplay] = useState(false)

const handleDelete = (i) => {
    console.log('delete', i)
    const temp = todoList
    temp.splice(i, 1)
    setTodo([...temp]);
}

const handleCompleted = (i) => {
  const temp = todoList
  if (temp[i].completed === false) {
    temp[i].completed = true
    setTodo([...temp])
  }
  else if (temp[i].completed === true) {
    temp[i].completed = false
    setTodo([...temp])
  }
  console.log(todoList)
}
    //use on change to send input data to parent

  const handleMouseEnter = () => {
    var temp = display
    temp = true
    setDisplay(temp)
  }
  const handleMouseLeave = () => {
      var temp = display
      temp = false
      setDisplay(temp)
  }
 
return(

    
        <div onMouseEnter={handleMouseEnter}  onMouseLeave={handleMouseLeave} className="todoWrapper">
        {display ? <i id="trash" class="far fa-trash-alt" onClick = {() => handleDelete(index)}></i> : null}
        {singleItem.completed === true ? <p id="todo" className="lineThrough">{singleItem.name}</p> : <p id="todo"> {singleItem.name} </p>}   
        <p id="done" onClick = {() =>handleCompleted(index) }>Done</p>
        </div>
       

);
}

export default SingleTodo
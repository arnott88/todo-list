import React, { useState } from 'react';
import './App.css';
import TodoList from './TodoList.js';
import SingleTodo from './SingleTodo.js';
import Input from './input.js';

function App() {

  const [todoList, setTodo] = useState([])
  const [newTodo, setNewTodo] = useState('')



  console.log('todo list =',todoList)

  console.log('new todo =',newTodo)


  return (
    <div className="App">
    
      <Input todoList={todoList} setTodo={setTodo} newTodo={newTodo} setNewTodo={setNewTodo} />
      <TodoList todoList ={todoList} setTodo={setTodo} />
       
      
    </div>
  );
}

export default App;

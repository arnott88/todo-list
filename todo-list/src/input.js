import React from 'react'
import TodoList from './TodoList'

const Input = (props) => {
    const { todoList, setTodo, newTodo, setNewTodo } = props
    

    const handleTodoChange = (e) => {
        setNewTodo(e.target.value)
      }
      
        const handleAdd = (e) => {
        todoList.push({name: newTodo, completed: false})
        setTodo(todoList)
        setNewTodo("")   
      }

const handleSubmit = e => {
    e.preventDefault()
    console.log(e)
   return newTodo === ''? null : handleAdd(e)
}

 
return(
    
    <form onSubmit={handleSubmit}>
       <div className="inputWrapper">
           <input type="text" id="input" placeholder="  Enter Todo..." value={newTodo} onChange={handleTodoChange} size="35"/> 
           <button id="button" className="enter">+</button>  
        </div>
    </form>
    
);
}

export default Input
import React from 'react'
import SingleTodo from './SingleTodo.js'
import App from './App.js';

const TodoList = (props) => {
    const { todoList, setTodo } = props



  
    //use on change to send input data to parent
 
return(

      <div>  
    {
        todoList.map((ele, i )=>{
          return <SingleTodo index={i} todoList={todoList} setTodo={setTodo} key ={i} singleItem= {ele}/>                                                         
                   })
      }
</div>
);
}

export default TodoList